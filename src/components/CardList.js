import React from 'react';
import Card from './Card';

const CardList = ({ cards, flipCard }) => {
  const cardColors = ['blue', 'red', 'green', 'yellow', 'purple', 'gray'];

  return (
    <div className="card-list">
      {cards.map((card, index) => (
        <Card
          key={card.id}
          id={card.id}
          isFlipped={card.isFlipped}
          onClick={flipCard}
          color={cardColors[index]}
        />
      ))}
    </div>
  );
};

export default CardList;
