// src/components/Card.js
import React, { useState } from "react";
import styled from "styled-components";
import ReactCardFlip from "react-card-flip";

const CardContainer = styled.div`
  width: 150px;
  height: 200px;
  margin: 10px;
  perspective: 1000px;
`;

const CardInner = styled.div`
  width: 100%;
  height: 100%;
  transform-style: preserve-3d;
  transition: transform 0.5s;
  transform: ${(props) => (props.isFlipped ? "rotateY(180deg)" : "none")};
`;

const CardFront = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  backface-visibility: hidden;
  background-color: ${(props) => props.color || "lightblue"};
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 24px;
`;

const CardBack = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  backface-visibility: hidden;
  background-color: ${(props) => props.color || "lightgray"};
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 24px;
  transform: rotateY(180deg);
`;

const Card = ({ color, isFlipped, handleClick }) => {
  return (
    <CardContainer onClick={handleClick}>
      <ReactCardFlip isFlipped={isFlipped} flipDirection="horizontal">
        <CardInner isFlipped={isFlipped}>
          <CardFront color={color}>Front</CardFront>
          <CardBack color={color}>Back</CardBack>
        </CardInner>
      </ReactCardFlip>
    </CardContainer>
  );
};

export default Card;
