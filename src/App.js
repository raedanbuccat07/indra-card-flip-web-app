import { useState, useEffect, Fragment, useCallback, useRef } from "react";
import { Container, Row, Col, Button, Modal, Table, Accordion, Form, Tab, Tabs, CardGroup, Card, Image } from "react-bootstrap";
import Swal from "sweetalert2/dist/sweetalert2.all.min.js";
import { Howl } from "howler";
import Flip from "./audioclips/Flip.mp3";
import Tada from "./audioclips/Tada.mp3";
import ReactCanvasConfetti from "react-canvas-confetti";
import * as xlsx from "xlsx";
import "./App.css";
import { Scrollbars } from "react-custom-scrollbars-2";
import CardBack from "./images/card_back.png"
import Sparkle from 'react-sparkle'
import 'animate.css';
import logoImage from './images/cardLogo.png';
import backgroundDefault from './images/cardbg.jpg';
import defaultBackground from './images/cardbg.jpg';

const canvasStyles = {
  position: "fixed",
  pointerEvents: "none",
  width: "100%",
  height: "100%",
  top: 0,
  left: 0,
  zIndex: 999
};

/*
function importAll(r) {
  let images = {};
  r.keys().map((item) => { images[item.replace('./', '')] = r(item); });
  return images;
}
*/

function importAll(r) {
  return r.keys().map(r);
};

const Cardz = ({ imageUrlBack, imageUrlFront, text, flipped, onClick }) => (
  <div
    className={`card-casino ${flipped ? 'flipped' : ''}`}
    style={{
      backgroundImage: `url(${flipped ? imageUrlBack : imageUrlFront})`,
      backgroundSize: 'contain',
      backgroundPosition: 'center',
      backgroundRepeat: "no-repeat",
    }}
    onClick={onClick}
  >
    <div className="card-content">
      <div
        className="card-front"

      >
        <div className="card-text"><h1>{flipped ? '' : ''}</h1></div>
      </div>
    </div>
  </div>
);

const App = () => {
  const [randomizerData, setRandomizerData] = useState([]);
  const [randomizerUsed, setRandomizerUsed] = useState([]);
  const [randomizerDataOriginal, setRandomizerDataOriginal] = useState([]);
  const [randomizerDisplay, setRandomizerDisplay] = useState([]);
  const [excludedNumber, setExcludedNumber] = useState([]);
  const [modalShow, setModalShow] = useState(false);
  const [backgroundImage, setBackgroundImage] = useState("");
  const refAnimationInstance = useRef(null);
  const [arrayKeyNames, setArrayKeyNames] = useState([]);
  const [firstData, setFirstData] = useState();
  const [secondData, setSecondData] = useState();
  const [backgroundBlur, setBackgroundBlur] = useState(0);
  //card
  const [allRevealed, setAllRevealed] = useState(false);
  const [firstDraw, setFirstDraw] = useState(false);
  const [cards, setCards] = useState([]);

  const zero = importAll(require.context('./images/00/', false, /\.(png|jpe?g|svg)$/));
  const one = importAll(require.context('./images/11/', false, /\.(png|jpe?g|svg)$/));
  const two = importAll(require.context('./images/22/', false, /\.(png|jpe?g|svg)$/));
  const three = importAll(require.context('./images/33/', false, /\.(png|jpe?g|svg)$/));
  const four = importAll(require.context('./images/44/', false, /\.(png|jpe?g|svg)$/));
  const five = importAll(require.context('./images/55/', false, /\.(png|jpe?g|svg)$/));
  const six = importAll(require.context('./images/66/', false, /\.(png|jpe?g|svg)$/));
  const seven = importAll(require.context('./images/77/', false, /\.(png|jpe?g|svg)$/));
  const eight = importAll(require.context('./images/88/', false, /\.(png|jpe?g|svg)$/));
  const nine = importAll(require.context('./images/99/', false, /\.(png|jpe?g|svg)$/));


  const getInstance = useCallback((instance) => {
    refAnimationInstance.current = instance;
  }, []);

  const makeShot = useCallback((particleRatio, opts) => {
    refAnimationInstance.current &&
      refAnimationInstance.current({
        ...opts,
        origin: { y: 0.7 },
        particleCount: Math.floor(200 * particleRatio),
      });
  }, []);

  const fire = useCallback(() => {
    makeShot(0.25, {
      spread: 26,
      startVelocity: 55,
    });

    makeShot(0.2, {
      spread: 60,
    });

    makeShot(0.35, {
      spread: 100,
      decay: 0.91,
      scalar: 0.8,
    });

    makeShot(0.1, {
      spread: 120,
      startVelocity: 25,
      decay: 0.92,
      scalar: 1.2,
    });

    makeShot(0.1, {
      spread: 120,
      startVelocity: 45,
    });
  }, [makeShot]);

  // Employee Randomizer
  async function dataRandomizer(randomInt) {
    setRandomizerDisplay(randomizerData[randomInt]);
    setRandomizerUsed([...randomizerUsed, randomizerData[randomInt]]);

    var arrayRandom = randomizerData[randomInt];
    var string = arrayRandom[firstData]?.toString().slice(0, 6).padEnd(6, '7');

    if (string === undefined) {
      string = "777777"
    }

    var array = Object.assign([], string);

    var initialCards = [];

    array.forEach((data, index) => {

      var imageSrc = "";
      var imgArray;
      var randomInt = 0;

      switch (data) {
        case "0":
          imgArray = zero;
          break;
        case "1":
          imgArray = one;
          break;
        case "2":
          imgArray = two;
          break;
        case "3":
          imgArray = three;
          break;
        case "4":
          imgArray = four;
          break;
        case "5":
          imgArray = five;
          break;
        case "6":
          imgArray = six;
          break;
        case "7":
          imgArray = seven;
          break;
        case "8":
          imgArray = eight;
          break;
        case "9":
          imgArray = nine;
          break;
        default:
          break;
      }

      randomInt = Math.floor(Math.random() * imgArray.length);
      imageSrc = imgArray[randomInt];

      initialCards.push({
        id: index,
        imageUrlBack: CardBack,
        imageUrlFront: imageSrc,
        text: data,
        flipped: true,
      })
    });

    setCards(initialCards);
  };

  const drawRandomizerData = async () => {
    if (firstData?.length === undefined || secondData?.length === undefined) {
      Swal.fire({
        icon: "error",
        title: "Select Data",
        text: "Please select data in the options before starting.",
      });

      return;
    }

    let randomInt = Math.floor(Math.random() * randomizerData.length);

    if (randomizerData?.length === 0) {
      Swal.fire({
        icon: "error",
        title: "Randomizer Data List Empty",
        text: "Please upload excel data in the options before starting.",
      });

      return;
    } else {
      do {
        if (excludedNumber.includes(randomInt)) {
          randomInt = Math.floor(Math.random() * randomizerData.length);
        }

        if (excludedNumber.length === randomizerData.length) {
          Swal.fire({
            icon: "error",
            title: "Reset",
            text: "All randomizer data has appeared. Please reset the list.",
          });

          return;
        }

        if (!excludedNumber.includes(randomInt)) {
          setExcludedNumber([...excludedNumber, randomInt]);
          dataRandomizer(randomInt);
        }
      } while (excludedNumber.includes(randomInt));
    }

    setFirstDraw(true);
    setAllRevealed(false);
  };

  const winnerList = async () => {
    setModalShow(true);
  };

  const employeeReset = async () => {
    Swal.fire({
      title: "Are you sure you want to reset?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "RESET",
    }).then((result) => {
      if (result.isConfirmed) {
        setRandomizerData(randomizerDataOriginal);
        setExcludedNumber([]);
        setRandomizerDisplay([]);
        setRandomizerUsed([]);
        setAllRevealed(false);
        setFirstDraw(false);
        setModalShow(false);
        setArrayKeyNames([]);
        setAllRevealed(false);
        setFirstDraw(false);
        setCards([]);
        Swal.fire({
          position: "center",
          icon: "success",
          title: "Reset",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    });
  };

  const readUploadFile = async (e) => {
    e.preventDefault();

    if (e.target.files) {
      const reader = new FileReader();
      reader.onload = (e) => {
        const data = e.target.result;
        const workbook = xlsx.read(data, { type: "array" });
        const sheetName = workbook.SheetNames[0];
        const worksheet = workbook.Sheets[sheetName];
        const json = xlsx.utils.sheet_to_json(worksheet);

        let tempData = json;
        let dataKeyNames = Object.keys(tempData[0]);

        setArrayKeyNames(dataKeyNames);

        tempData.map((data, i) => {
          return (tempData[i].isUsed = false);
        });

        setRandomizerData(tempData);
        setRandomizerDataOriginal(tempData);

        Swal.fire({
          position: "center",
          icon: "success",
          title: "Uploaded Data",
          showConfirmButton: false,
          timer: 1500,
        });

        document.getElementById("uploadData").value = "";
      };
      reader.readAsArrayBuffer(e.target.files[0]);
    }
  };

  const SoundPlay = (src) => {
    const sound = new Howl({
      src,
      html5: true,
    });
    sound.play();
  };

  const checkboxClick = (data, type) => {
    switch (type) {
      case 1:
        setFirstData(data);
        break;
      case 2:
        setSecondData(data);
        break;
      default:
    }
  };

  const flipCard = (id) => {
    const updatedCards = cards.map((card) =>
      card.id === id ? { ...card, flipped: !card.flipped } : card
    );

    setCards(updatedCards);

    var falseCount = 0;

    updatedCards.forEach((data) => {
      if (data.flipped === false) {
        falseCount++;
      }
    });

    if (updatedCards.length === falseCount) {
      setAllRevealed(true);
      fire();
      SoundPlay(Tada);
    } else {
      setAllRevealed(false);
    }

    SoundPlay(Flip);

  };

  const flipAllCards = () => {
    const updatedCards = cards.map((card) => ({ ...card, flipped: false }));

    if (!allRevealed) {
      SoundPlay(Flip);
    }

    setAllRevealed(true);
    fire();
    SoundPlay(Tada);
    setCards(updatedCards);
  };

  const resetAllCards = () => {
    var trueCount = 0;

    cards.forEach((data) => {
      if (data.flipped === false) {
        trueCount++;
      }
    });

    if (trueCount > 0) {
      SoundPlay(Flip);
    }

    const updatedCards = cards.map((card) => ({ ...card, flipped: true }));
    setCards(updatedCards);
    setAllRevealed(false);
  };

  const drawWinner = () => {
    SoundPlay(Flip);

    setAllRevealed(false);
    drawRandomizerData();
  }

  const addBlurBackground = () => {
    setBackgroundBlur(backgroundBlur + 1);
  };

  const reduceBlurBackground = () => {
    if (backgroundBlur <= 0) {
      return
    } else {
      setBackgroundBlur(backgroundBlur - 1);
    }
  };

  const removeBlurBackground = () => {
    setBackgroundBlur(0);
  };

  const setDefaultBackground = () => {
    setBackgroundImage(defaultBackground);
  };

  const removeBackground = () => {
    setBackgroundImage();
  };

  useEffect(() => {
    const fileInput = document.getElementById("fileInput");
    fileInput?.addEventListener("change", (e) => {
      const file = fileInput?.files[0];
      const reader = new FileReader();
      reader?.addEventListener("load", () => {
        setBackgroundImage(reader?.result);
      });
      if (file) {
        reader?.readAsDataURL(file);
      }
      document.getElementById("fileInput").value = "";
    });
  }, [firstData, cards, firstDraw, modalShow, allRevealed]);

  return (
    <Fragment>
      <Scrollbars
        autoHide
        autoHideTimeout={1000}
        autoHideDuration={200}
      >
        {
          allRevealed ?
            (<Sparkle
              minSize={10}
              maxSize={30}
              flickerSpeed={"fast"}
              overflowPx={-10}
            />)
            :
            (<Sparkle
              minSize={1}
              maxSize={10}
              overflowPx={-10}
            />)
        }

        <div className="lottery-container2 w-100 h-100" style={{ backgroundImage: `url(${backgroundImage ? backgroundImage : backgroundDefault})`, filter: `blur(${backgroundBlur}px)`, WebkitFilter: `blur(${backgroundBlur}px)` }}></div>
        <Container
          fluid
          className="lottery-container h-100 m-0 p-0"
        >
          <Row className="h-35 m-0 p-0 d-flex align-items-center justify-content-center">
            <Col xs={12} className="h-100 logo-box">
              <Image src={logoImage} className="card-logo" />
            </Col>
          </Row>
          <Row className="h-35 m-0 p-0">
            <Col className="d-flex align-items-center justify-content-center col-12 pt-3">
              {
                firstDraw ? (
                  <CardGroup className="h-100 w-100 pt-2">
                    {
                      cards.map((data, index) => {
                        return (
                          <Card key={data.id + index} className="animate__animated animate__fadeInLeft p-0 m-0 border-0">
                            <Card.Body className="p-0 m-0">
                              <Cardz
                                key={data.id}
                                imageUrlBack={data.imageUrlBack}
                                imageUrlFront={data.imageUrlFront}
                                text={data.text}
                                flipped={data.flipped}
                                onClick={() => flipCard(data.id)}
                              />
                            </Card.Body>
                          </Card>
                        );
                      })
                    }
                  </CardGroup>
                ) : (<></>)
              }
            </Col>
          </Row>
          <Row className="h-20 m-0 p-0">
            <Col className={`col-12 d-flex justify-content-center align-items-start m-0 p-0`}>
              {
                allRevealed ?
                  (
                    <pre className="pt-3 p-0 m-0 animate__animated animate__flipInX animate__fast">
                      <h1 className="first-data text-center">{randomizerDisplay[firstData]}</h1>
                      <h1 className="second-data text-uppercase text-center">{randomizerDisplay[secondData]}</h1>
                    </pre>
                  ) : (<></>)
              }
            </Col>
          </Row>

          <ReactCanvasConfetti refConfetti={getInstance} style={canvasStyles} />

          <Row className="h-10 m-0 p-0">
            <Col className={`col-12 d-flex justify-content-center align-items-center`}>
              <Button
                className="card-button text-uppercase flip-button-color"
                size="sm"
                variant="light"
                onClick={!allRevealed ? flipAllCards : resetAllCards}
                disabled={cards.length === 0 ? true : false}
                active
              >
                {!allRevealed ? "Flip Cards" : "Unflip Cards"}
              </Button>
              {
                firstDraw ? (
                  <Button
                    className="card-button text-uppercase draw-button-color mx-5"
                    size="sm"
                    variant="light"
                    onClick={drawWinner}
                    active
                    disabled={!allRevealed}
                  >
                    Draw
                  </Button>
                ) :
                  (
                    <Button
                      className="card-button text-uppercase draw-button-color mx-5"
                      size="sm"
                      variant="light"
                      onClick={drawWinner}
                      active
                    >
                      Draw
                    </Button>
                  )
              }
            </Col>
          </Row>
          <Accordion>
            <Accordion.Item eventKey="0">
              <Accordion.Header>OPTIONS (CLICK ME)</Accordion.Header>
              <Accordion.Body className="accordion-bg">
                <Tabs defaultActiveKey="file" className="mb-3">
                  <Tab eventKey="file" title="Upload File">
                    <Row>
                      <Col className="text-end">
                        <form>
                          <label
                            htmlFor="uploadData"
                            style={{
                              cursor: "pointer",
                              background: "grey",
                              padding: "5px",
                              color: "white",
                            }}
                          >
                            Upload Form Data
                          </label>
                          <input
                            className="d-none"
                            style={{ visibility: "hidden" }}
                            type="file"
                            name="uploadData"
                            id="uploadData"
                            onChange={readUploadFile}
                          />
                        </form>
                      </Col>
                      <Col className="text-start ">
                        <form>
                          <label
                            htmlFor="fileInput"
                            style={{
                              cursor: "pointer",
                              background: "grey",
                              padding: "5px",
                              color: "white",
                            }}
                          >
                            Set Background Image
                          </label>
                          <input
                            className="d-none"
                            style={{ visibility: "hidden" }}
                            type="file"
                            name="fileInput"
                            id="fileInput"
                          />
                        </form>
                      </Col>
                      <Col className="text-start">
                        <Button variant="info" onClick={() => setDefaultBackground()}>Add Default Background</Button>
                      </Col>
                      <Col className="text-start">
                        <Button variant="dark" onClick={() => removeBackground()}>Remove Background</Button>
                      </Col>
                      <Col className="text-start">
                        <Button variant="success" onClick={() => addBlurBackground()}>Add Blur</Button>
                        <Button variant="primary" onClick={() => reduceBlurBackground()}>Reduce Blur</Button>
                        <Button variant="danger" onClick={() => removeBlurBackground()}>Remove Blur</Button>
                      </Col>
                    </Row>
                  </Tab>
                  <Tab eventKey="data" title="Select Data (IMPORTANT)">
                    <Row className="m-0 p-0">
                      <Col xs={12} className="radio-section">
                        {arrayKeyNames?.length !== 0 ? (
                          <Form className="d-flex justify-content-center">
                            <div className="m-3 text-center">
                              <h3>1st Data: </h3>
                              {arrayKeyNames?.map((data) => {
                                return (
                                  <Form.Check
                                    inline
                                    label={data}
                                    name="group1"
                                    type="radio"
                                    id={`inline-${data}`}
                                    key={data}
                                    onClick={() => checkboxClick(data, 1)}
                                  />
                                );
                              })}
                            </div>
                          </Form>
                        ) : (
                          <h3 className="text-center">Empty Data</h3>
                        )}
                      </Col>
                      <Col xs={12} className="radio-section">
                        {arrayKeyNames?.length !== 0 ? (
                          <Form className="d-flex justify-content-center">
                            <div className="m-3 text-center">
                              <h3>2nd Data: </h3>
                              {arrayKeyNames?.map((data) => {
                                return (
                                  <Form.Check
                                    inline
                                    label={data}
                                    name="group2"
                                    type="radio"
                                    id={`inline-${data}`}
                                    key={data}
                                    onClick={() => checkboxClick(data, 2)}
                                  />
                                );
                              })}
                            </div>
                          </Form>
                        ) : (
                          <></>
                        )}
                      </Col>
                    </Row>
                  </Tab>
                  <Tab eventKey="drawlist" title="Draw List/Reset">
                    <Row>
                      <Col className="text-end">
                        <Button
                          size="lg"
                          variant="light"
                          className="winner-list-button border-radius-button"
                          onClick={() => winnerList()}
                          active
                        >
                          DRAW LIST
                        </Button>
                      </Col>
                      <Col className="text-start">
                        <Button
                          size="lg"
                          variant="light"
                          className="reset-button border-radius-button"
                          onClick={() => employeeReset()}
                          active
                        >
                          RESET
                        </Button>
                      </Col>
                    </Row>
                  </Tab>
                  <Tab eventKey="loadCards" title="Load Cards">
                    <Row className="w-100 h-100 p-0 m-0">
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          <Card>
                            <Card.Img variant="top" src={CardBack} />
                          </Card>
                          {
                            zero.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          {
                            one.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          {
                            two.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          {
                            three.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          {
                            four.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          {
                            five.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          {
                            six.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          {
                            seven.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          {
                            eight.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          {
                            nine.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                    </Row>
                  </Tab>
                </Tabs>
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
        </Container>
      </Scrollbars>
      <Modal
        show={modalShow}
        onHide={() => setModalShow(false)}
        size="lg"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            <h1 className="text-center">
              Raffle Draw List - Total Draw: {randomizerUsed.length}
            </h1>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Table striped bordered hover variant="dark">
            <thead>
              <tr>
                <th>{firstData}</th>
                <th>{secondData}</th>
              </tr>
            </thead>
            <tbody>
              {randomizerUsed.length > 0 ? (
                randomizerUsed?.map((data, i) => (
                  <tr key={i}>
                    <td>
                      {i + 1} - {data[firstData]}
                    </td>
                    <td>{data[secondData]}</td>
                  </tr>
                ))
              ) : (
                <></>
              )}
            </tbody>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => setModalShow(false)}>Close</Button>
        </Modal.Footer>
      </Modal>
    </Fragment >

  );
};

export default App;
